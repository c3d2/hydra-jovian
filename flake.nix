{
  description = "A very basic flake";

  inputs = {
    jovian = {
      url = "github:Jovian-Experiments/Jovian-NixOS";
      flake = false;
    };

    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, jovian, nixpkgs }:
    let
      inherit (nixpkgs) lib;

      nixosSystem' = nixpkgs: nixpkgs.lib.nixosSystem {
        modules = [
          "${jovian}/modules"
          "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-graphical-calamares-plasma5.nix"

          ({ config, ... }: {
            # avoid asserts ...
            boot.loader.grub.devices = [ "/dev/sda" ];
            fileSystems."/".device = "/dev/sda1";

            # ... and warnings ...
            networking.networkmanager.enable = true;
            system.stateVersion = "23.05";

            # ... and build unfree packages
            nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
              "steam"
              "steam-jupiter-original"
              "steam-run"
              "steamdeck-hw-theme"
            ];

            jovian = {
              devices.steamdeck.enable = true;
              steam.enable = true;
            };

            # copied from upstream to build iso
            # https://github.com/Jovian-Experiments/Jovian-NixOS/blob/development/default.nix#L19-L21
            hardware.pulseaudio.enable = lib.mkIf
              (config.jovian.devices.steamdeck.enableSoundSupport && config.services.pipewire.enable)
              (lib.mkForce false);
          })
        ];

        system = "x86_64-linux";
      };
    in
    {
      nixosConfigurations.jovian = nixosSystem' nixpkgs;

      hydraJobs = {
        iso = lib.hydraJob self.nixosConfigurations.jovian.config.system.build.isoImage;
        system = lib.hydraJob self.nixosConfigurations.jovian.config.system.build.toplevel;
      };
    };
}
