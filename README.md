# Jovian

Repo to build nixos for jovian on a hydra.

See https://github.com/Jovian-Experiments/Jovian-NixOS for instructions and https://hydra.hq.c3d2.de/jobset/c3d2/jovian for jobset on hydra.
